#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <string.h>
#include <time.h>
#include <fcntl.h>
#include <errno.h>
#include <dirent.h>
#include <wait.h>
#include <signal.h>
#include <stdbool.h>

#define NUM_IMAGES 15
#define INTERVAL 30
#define MODE_A 1
#define MODE_B 2

bool is_running = true;
int mode;

//fungsi untuk download gambar
void download_image(const char* folder_name, int thread_num) {
    time_t t = time(NULL);
    char img_name[50];
    sprintf(img_name, "%04d-%02d-%02d_%02d:%02d:%02d_%d.jpg",
        1900 + localtime(&t)->tm_year, localtime(&t)->tm_mon + 1, localtime(&t)->tm_mday,
        localtime(&t)->tm_hour, localtime(&t)->tm_min, localtime(&t)->tm_sec, thread_num);
    char url[50];
    sprintf(url, "https://picsum.photos/%d", (int) (t%1000)+50);
    char path[100];
    sprintf(path, "%s/%s", folder_name, img_name);

    pid_t pid = fork();
    if (pid == 0) {
        execl("/usr/bin/wget", "wget", "-q", "-O", path, url, NULL);
        exit(EXIT_SUCCESS);
    }
    else if (pid < 0) {
        perror("Failed to create child process");
        exit(EXIT_FAILURE);
    }
}

//fungsi untuk membuat program killer
void create_killer(char* program_name) {
    FILE* fp;
    fp = fopen("killer", "w");
    fprintf(fp, "#!/bin/bash\n");

    if (mode == MODE_A) {
        fprintf(fp, "killall -SIGKILL %s\n", program_name);
    } else {
        fprintf(fp, "kill -SIGTERM %d\n", getpid());
    }

    fprintf(fp, "rm killer\n");
    fclose(fp);
    pid_t pid = fork();
    if (pid == 0) {
        execl("/bin/chmod", "chmod", "+x", "killer", NULL);
        exit(EXIT_SUCCESS);
    }
} 

//membuat fungsi untuk zipping folder
void zip_folder(const char* folder_name) {
    char zip_name[100];
    sprintf(zip_name, "%s.zip", folder_name);

    pid_t pid = fork();
    if (pid == 0) {
        execl("/usr/bin/zip", "zip", "-rmq", zip_name, folder_name, NULL);
        perror("Failed to compress folder");
        exit(EXIT_FAILURE);
    }
    else if (pid < 0) {
        perror("Failed to create child process");
        exit(EXIT_FAILURE);
    }
    else {
        int status;
        waitpid(pid, &status, 0);
        if (WIFEXITED(status) && WEXITSTATUS(status) == 0) {
            printf("Folder %s has been compressed successfully.\n", folder_name);
        }
        else {
            perror("Failed to compress folder");
            exit(EXIT_FAILURE);
        }
    }
}

//membuat fungsi untuk delete folder
void delete_folder(const char* folder_name) {
    pid_t pid = fork();
    if (pid == 0) {
        execl("/bin/rm", "rm", "-rf", folder_name, NULL);
        perror("Failed to delete folder");
        exit(EXIT_FAILURE);
    }
    else if (pid < 0) {
        perror("Failed to create child process");
        exit(EXIT_FAILURE);
    }
    else {
        int status;
        waitpid(pid, &status, 0);
        if (WIFEXITED(status) && WEXITSTATUS(status) == 0) {
            printf("Folder %s has been deleted successfully.\n", folder_name);
        }
        else {
            perror("Failed to delete folder");
            exit(EXIT_FAILURE);
        }
    }
}

//membuat fungsi sigkill handler
void sigkill_handler(int sig) {
    is_running = false;
}

//fungsi utama
int main(int argc, char** argv) {
    if (argc < 2) { // jika jumlah argumen kurang dari 2, tampilkan pesan error dan keluar dari program
        printf("Usage: %s [-a | -b]\n", argv[0]);
        exit(EXIT_FAILURE);
    }
    if (strcmp(argv[1], "-a") == 0) { // jika argumen kedua sama dengan "-a", set mode ke MODE_A
        mode = MODE_A;
    } else if (strcmp(argv[1], "-b") == 0) { // jika argumen kedua sama dengan "-b", set mode ke MODE_B
        mode = MODE_B;
    } else { // jika tidak, tampilkan pesan error dan keluar dari program
        printf("Invalid argument\n");
        exit(EXIT_FAILURE);
    }

    signal(SIGKILL, sigkill_handler); // menangani sinyal SIGKILL dengan fungsi sigkill_handler

    pid_t pid, sid;
    
    pid = fork(); // membuat proses baru
    if (pid < 0) { // jika gagal membuat proses baru, keluar dari program
        exit(EXIT_FAILURE);
    }
    if (pid > 0) { // jika proses berhasil dibuat, keluar dari program
        exit(EXIT_SUCCESS);
    }

    umask(0); // mengubah hak akses file

    sid = setsid(); // membuat sesi baru

    if (sid < 0) { // jika gagal membuat sesi baru, keluar dari program
        exit(EXIT_FAILURE);
    }

    if (chdir(".") < 0) { // mengubah direktori kerja ke direktori utama
        exit(EXIT_FAILURE);
    }

    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);

    create_killer(argv[0]); // membuat file killer

    while (is_running) { // selama program masih berjalan
        time_t t = time(NULL); // mendapatkan waktu saat ini
        char folder_name[50]; // deklarasi
        sprintf(folder_name, "%04d-%02d-%02d_%02d:%02d:%02d", 
            1900 + localtime(&t)->tm_year, localtime(&t)->tm_mon + 1, localtime(&t)->tm_mday,
            localtime(&t)->tm_hour, localtime(&t)->tm_min, localtime(&t)->tm_sec);

        pid_t child_pid = fork();

        if (child_pid == 0) {
            mkdir(folder_name, 0777);

            for (int i = 0; i < NUM_IMAGES; i++) {
                download_image(folder_name, i + 1);
                sleep(5);
            }
            zip_folder(folder_name);
            delete_folder(folder_name);
            exit(EXIT_SUCCESS);
            
        } else {
            sleep(INTERVAL);
        }
    }



    exit(EXIT_SUCCESS);
}