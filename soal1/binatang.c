#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <zip.h>
#include <curl/curl.h>
#include <dirent.h>

struct BufferStruct
{
    char *memory;
    size_t size;
};

static size_t WriteMemoryCallback(void *contents, size_t size, size_t nmemb, void *userp)
{
    size_t realsize = size * nmemb;
    struct BufferStruct *mem = (struct BufferStruct *)userp;

    mem->memory = (char *)realloc(mem->memory, mem->size + realsize + 1);
    if (mem->memory == NULL)
    {
        printf("Not enough memory (realloc returned NULL)\n");
        return 0;
    }

    memcpy(&(mem->memory[mem->size]), contents, realsize);
    mem->size += realsize;
    mem->memory[mem->size] = 0;

    return realsize;
}

int download_file(char *url, char *filename)
{
    CURL *curl_handle;
    CURLcode res;
    struct BufferStruct output;

    output.memory = NULL;
    output.size = 0;

    curl_global_init(CURL_GLOBAL_ALL);
    curl_handle = curl_easy_init();
    if (curl_handle)
    {
        curl_easy_setopt(curl_handle, CURLOPT_URL, url);
        curl_easy_setopt(curl_handle, CURLOPT_FOLLOWLOCATION, 1L);
        curl_easy_setopt(curl_handle, CURLOPT_WRITEFUNCTION, WriteMemoryCallback);
        curl_easy_setopt(curl_handle, CURLOPT_WRITEDATA, (void *)&output);

        res = curl_easy_perform(curl_handle);

        curl_easy_cleanup(curl_handle);

        if (res != CURLE_OK)
        {
            printf("curl_easy_perform() failed: %s\n", curl_easy_strerror(res));
            return 0;
        }
        else
        {
            FILE *fp = fopen(filename, "wb");
            if (fp == NULL)
            {
                printf("Failed to create file %s\n", filename);
                return 0;
            }
            else
            {
                fwrite(output.memory, 1, output.size, fp);
                fclose(fp);
                free(output.memory);
                return 1;
            }
        }
    }
    return 0;
}

// fungsi untuk melakukan ekstraksi file zip
int unzip(char *zipfilename, char *dirname)
{
    struct zip *za;
    if ((za = zip_open(zipfilename, 0, NULL)) == NULL)
    {
        return 0;
    }

    int numFiles = zip_get_num_entries(za, 0);
    for (int i = 0; i < numFiles; i++)
    {
        struct zip_stat sb;
        if (zip_stat_index(za, i, 0, &sb) == 0)
        {
            const char *filename = zip_get_name(za, i, ZIP_FL_UNCHANGED);

            char filepath[256];
            sprintf(filepath, "%s/%s", dirname, filename);

            if (sb.size > 0)
            {
                char *buf = (char *)malloc(sb.size);
                struct zip_file *zf = zip_fopen_index(za, i, 0);
                if (zf == NULL)
                {
                    zip_close(za);
                    return 0;
                }
                zip_fread(zf, buf, sb.size);
                zip_fclose(zf);

                FILE *fp = fopen(filepath, "wb");
                if (fp == NULL)
                {
                    free(buf);
                    zip_close(za);
                    return 0;
                }
                fwrite(buf, sb.size, 1, fp);
                fclose(fp);
                free(buf);
            }
            else
            {
#ifdef _WIN32
                _mkdir(filepath);
#else
                mkdir(filepath, 0755);
#endif
            }
        }
    }

    zip_close(za);
    return 1;
}

int zip(char *zipfilename, char *dirname)
{
    struct zip *za;
    if ((za = zip_open(zipfilename, ZIP_CREATE, NULL)) == NULL)
    {
        return 0;
    }

    DIR *d;
    struct dirent *dir;
    d = opendir(dirname);

    if (d)
    {
        while ((dir = readdir(d)) != NULL)
        {
            char *ext = strrchr(dir->d_name, '.');
            if (ext != NULL && (strcmp(ext, ".jpg") == 0 || strcmp(ext, ".jpeg") == 0 || strcmp(ext, ".png") == 0))
            {
                char filepath[256];
                snprintf(filepath, sizeof(filepath), "%s/%s", dirname, dir->d_name);

                struct stat st;
                stat(filepath, &st);

                if (S_ISREG(st.st_mode))
                {
                    struct zip_source *source = zip_source_file(za, filepath, 0, 0);
                    if (source == NULL)
                    {
                        zip_close(za);
                        return 0;
                    }

                    char zipfilepath[256];
                    snprintf(zipfilepath, sizeof(zipfilepath), "%s/%s", dirname, zipfilename);
                    int err = zip_file_add(za, dir->d_name, source, ZIP_FL_OVERWRITE);
                    if (err < 0)
                    {
                        zip_source_free(source);
                        zip_close(za);
                        return 0;
                    }
                }
            }
        }
        closedir(d);
    }

    zip_close(za);
    return 1;
}

char *choose_random_file()
{
    DIR *d;
    struct dirent *dir;
    char *files[1000];
    int count = 0;
    srand(time(NULL));

    // Open current directory
    d = opendir(".");
    if (d)
    {
        // Iterate through all files in directory
        while ((dir = readdir(d)) != NULL)
        {
            // Check if file has .jpg extension
            char *ext = strrchr(dir->d_name, '.');
            if (ext != NULL && strcmp(ext, ".jpg") == 0)
            {
                // Add file name to array of files
                files[count] = dir->d_name;
                count++;
            }
        }
        closedir(d);
    }
    else
    {
        printf("Error opening directory.\n");
        return NULL;
    }

    // If no .jpg files found, return NULL
    if (count == 0)
    {
        printf("No .jpg files found in directory.\n");
        return NULL;
    }

    // Choose a random file from the array
    int rand_index = rand() % count;
    char *rand_file = files[rand_index];
    return rand_file;
}

void make_directory(const char *dir_name)
{
#ifdef _WIN32
    _mkdir(dir_name);
#else
    mkdir(dir_name, 0755);
#endif
}

void move_file(const char *filename, const char *dest_dir)
{
    char dest_path[256];
    sprintf(dest_path, "%s/%s", dest_dir, filename);

    rename(filename, dest_path);
}

void sort_files_by_habitat()
{
    DIR *d;
    struct dirent *dir;
    d = opendir(".");
    if (d)
    {
        while ((dir = readdir(d)) != NULL)
        {
            char *ext = strrchr(dir->d_name, '.');
            if (ext != NULL && (strcmp(ext, ".jpg") == 0 || strcmp(ext, ".jpeg") == 0 || strcmp(ext, ".png") == 0))
            {
                char *habitat;
                if (strstr(dir->d_name, "_darat") != NULL)
                {
                    habitat = "HewanDarat";
                }
                else if (strstr(dir->d_name, "_amphibi") != NULL)
                {
                    habitat = "HewanAmphibi";
                }
                else if (strstr(dir->d_name, "_air") != NULL)
                {
                    habitat = "HewanAir";
                }
                else
                {
                    habitat = "Unknown";
                }

                make_directory(habitat);
                move_file(dir->d_name, habitat);
            }
        }
        closedir(d);
    }
}

int main(void)
{
    char *url = "https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq";
    char *filename = "animal.zip";
    int downloadResult = download_file(url, filename);

    if (downloadResult)
    {
        printf("File %s berhasil diunduh.\n", filename);
        int unzipResult = unzip(filename, ".");
        if (unzipResult)
        {
            printf("File %s berhasil diekstrak.\n", filename);
        }
        else
        {
            printf("Gagal mengekstrak file.\n");
        }
    }
    else
    {
        printf("Gagal mengunduh file.\n");
    }

    char *rand_file = choose_random_file();
    if (rand_file != NULL)
    {
        printf("Random file chosen: %s\n", rand_file);
    }

    sort_files_by_habitat();

    // // zip berdasarkan habitat

    zip("HewanDarat.zip", "HewanDarat");
    zip("HewanAmphibi.zip", "HewanAmphibi");
    zip("HewanAir.zip", "HewanAir");

    return 0;
}
