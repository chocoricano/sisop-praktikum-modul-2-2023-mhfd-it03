# sisop-praktikum-modul-2-2023-MH/Fd-IT03

## Laporan Resmi Modul 2 Praktikum Sistem Operasi 2023

---

### Kelompok IT03:
Sedtia Prakoso Budi Tirto Arto 5027211014

Yohannes Hasahatan Tua Alexandro 5027211022

Andyana Muhandhatul Nabila 5027211029

## SOAL 1

### Soal 1a

Grape-kun harus mendownload file tersebut untuk disimpan pada penyimpanan local komputernya. Dan untuk melakukan melihat file gambar pada folder yang telah didownload Grape-kun harus melakukan unzip pada folder tersebut.

### Analisis soal:

Pada soal nomor 1a diminta untuk mendownload file yang diberikan untuk disimpan pada penyimpanan local komputer dan melakukan unzip pada folder tersebut.

### Cara penyelesaian soal 1a:

Dalam penyelesaian dari soal ini adalah digunakan curl untuk mendownload file nya dalam soal ini fungsi yang digunakakan adalah download_file. kemudian fungsi ini akan dipanggil dalam main dimana file yang di unduh akan diberikan nama animal.zip. cara menjalankannya adalah yang pertama di compile "gcc binatang.c -o binatang -lzip -lcurl" kemudian file dijalankan

### Kendala

kendala yang ditemukan dalam soal nomer satu adalah compiler gcc lupa belum di install sehingga program tidak dapat dicompile dan dijalankan

### Source code
Fungsi download_file
```sh
int download_file(char *url, char *filename)
{
    CURL *curl_handle;
    CURLcode res;
    struct BufferStruct output;

    output.memory = NULL;
    output.size = 0;

    curl_global_init(CURL_GLOBAL_ALL);
    curl_handle = curl_easy_init();
    if (curl_handle)
    {
        curl_easy_setopt(curl_handle, CURLOPT_URL, url);
        curl_easy_setopt(curl_handle, CURLOPT_FOLLOWLOCATION, 1L);
        curl_easy_setopt(curl_handle, CURLOPT_WRITEFUNCTION, WriteMemoryCallback);
        curl_easy_setopt(curl_handle, CURLOPT_WRITEDATA, (void *)&output);

        res = curl_easy_perform(curl_handle);

        curl_easy_cleanup(curl_handle);

        if (res != CURLE_OK)
        {
            printf("curl_easy_perform() failed: %s\n", curl_easy_strerror(res));
            return 0;
        }
        else
        {
            FILE *fp = fopen(filename, "wb");
            if (fp == NULL)
            {
                printf("Failed to create file %s\n", filename);
                return 0;
            }
            else
            {
                fwrite(output.memory, 1, output.size, fp);
                fclose(fp);
                free(output.memory);
                return 1;
            }
        }
    }
    return 0;
}
```
```sh
int main(void)
{
    char *url = "https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq";
    char *filename = "animal.zip";
    int downloadResult = download_file(url, filename);
```
### Test output

[![HNTer2s.jpg](https://iili.io/HNTer2s.jpg)](https://freeimage.host/id)

[![HNTep3u.jpg](https://iili.io/HNTep3u.jpg)](https://freeimage.host/id)

## Soal 1b
Setelah berhasil melakukan unzip Grape-kun melakukan pemilihan secara acak pada file gambar tersebut untuk melakukan shift penjagaan pada hewan tersebut.

## Analisis soal
melakukan pemilihan secara acak pada file gambar tersebut untuk melakukan shift penjagaan pada hewan tersebut

## Kendala
dalam soal ini kendala yang ditemukan adalah file acak tidak muncul/ terprint
## Cara penyelesaian soal 1b:
dalam menyelesaikan soal ini digunakan fungsi choose_random_file. Pertama-tama, fungsi ini akan membuka direktori saat ini menggunakan fungsi opendir(). Kemudian, fungsi akan membaca semua file dalam direktori dengan menggunakan loop while dan fungsi readdir(). Setiap kali fungsi readdir() dipanggil, ia akan mengembalikan struct dirent yang mewakili file atau direktori berikutnya di direktori yang dibuka.

Fungsi ini kemudian memeriksa apakah file yang sedang diproses memiliki ekstensi ".jpg" menggunakan fungsi strrchr() dan strcmp(). Jika file tersebut memiliki ekstensi yang benar, maka nama file akan ditambahkan ke dalam array files.

Setelah semua file dengan ekstensi ".jpg" telah diproses, direktori akan ditutup menggunakan fungsi closedir(). Kemudian, fungsi akan memeriksa apakah ada file dengan ekstensi ".jpg" di direktori. Jika tidak ada, fungsi akan mencetak pesan kesalahan dan mengembalikan NULL.

Jika ada setidaknya satu file dengan ekstensi ".jpg", fungsi akan memilih sebuah file secara acak dari array files menggunakan fungsi rand() dan time(NULL). Fungsi rand() akan menghasilkan angka acak dari 0 hingga RAND_MAX. Dalam kode di atas, rand_index akan menghasilkan indeks acak dari file-file yang ditemukan.

kemudian fungsi akan mengembalikan string yang berisi nama file yang dipilih secara acak.
## Source code 
```sh
char *choose_random_file()
{
    DIR *d;
    struct dirent *dir;
    char *files[1000];
    int count = 0;
    srand(time(NULL));

    // Open current directory
    d = opendir(".");
    if (d)
    {
        // Iterate through all files in directory
        while ((dir = readdir(d)) != NULL)
        {
            // Check if file has .jpg extension
            char *ext = strrchr(dir->d_name, '.');
            if (ext != NULL && strcmp(ext, ".jpg") == 0)
            {
                // Add file name to array of files
                files[count] = dir->d_name;
                count++;
            }
        }
        closedir(d);
    }
    else
    {
        printf("Error opening directory.\n");
        return NULL;
    }

    // If no .jpg files found, return NULL
    if (count == 0)
    {
        printf("No .jpg files found in directory.\n");
        return NULL;
    }

    // Choose a random file from the array
    int rand_index = rand() % count;
    char *rand_file = files[rand_index];
    return rand_file;
}
```
## Test output
[![HNTvYxV.png](https://iili.io/HNTvYxV.png)](https://freeimage.host/id)

## Soal 1c
Karena Grape-kun adalah orang yang perfeksionis Grape-kun ingin membuat direktori untuk memilah file gambar tersebut. Direktori tersebut dengan nama HewanDarat, HewanAmphibi, dan HewanAir. Setelah membuat direktori tersebut Grape-kun harus melakukan filter atau pemindahan file gambar hewan sesuai dengan tempat tinggal nya.

## Analisis soal:
membuat direktori untuk memilah file gambar tersebut. dengan nama HewanDarat, HewanAmphibi, dan HewanAir. kemudian melakukan filter atau pemindahan file gambar hewan sesuai dengan tempat tinggal nya.
## Kendala
terdapat kesalahan logika fungsi sehingga program tidak berjalan, dimana file tidak sesuai dengan ketentuan
## Cara penyelesaian soal 1c:
Dalam menyelesaikan soal ini digunakan fungsi sort_files_by_habitat, Fungsi ini menggunakan library dirent.h dan mengakses direktori saat ini dengan menggunakan fungsi opendir(). Kemudian, fungsi akan membaca semua file dalam direktori dengan menggunakan loop while dan fungsi readdir(). Setiap kali fungsi readdir() dipanggil, ia akan mengembalikan struct dirent yang mewakili file atau direktori berikutnya di direktori yang dibuka.

Fungsi ini kemudian memeriksa apakah file yang sedang diproses memiliki ekstensi ".jpg", ".jpeg", atau ".png" menggunakan fungsi strrchr() dan strcmp(). Jika file tersebut memiliki ekstensi yang benar, maka fungsi akan menentukan habitat hewan yang ada di dalam file tersebut. Habitat akan ditentukan berdasarkan nama file, misalnya jika nama file mengandung kata "_darat", maka habitatnya adalah "HewanDarat". Setelah itu, fungsi akan memindahkan file ke dalam folder yang sesuai dengan habitatnya menggunakan fungsi make_directory() dan move_file().
## Source code 
```sh
void sort_files_by_habitat()
{
    DIR *d;
    struct dirent *dir;
    d = opendir(".");
    if (d)
    {
        while ((dir = readdir(d)) != NULL)
        {
            char *ext = strrchr(dir->d_name, '.');
            if (ext != NULL && (strcmp(ext, ".jpg") == 0 || strcmp(ext, ".jpeg") == 0 || strcmp(ext, ".png") == 0))
            {
                char *habitat;
                if (strstr(dir->d_name, "_darat") != NULL)
                {
                    habitat = "HewanDarat";
                }
                else if (strstr(dir->d_name, "_amphibi") != NULL)
                {
                    habitat = "HewanAmphibi";
                }
                else if (strstr(dir->d_name, "_air") != NULL)
                {
                    habitat = "HewanAir";
                }
                else
                {
                    habitat = "Unknown";
                }

                make_directory(habitat);
                move_file(dir->d_name, habitat);
            }
        }
        closedir(d);
    }
}
```

## Test output
[![HNTv10F.png](https://iili.io/HNTv10F.png)](https://freeimage.host/id)

## Soal 1d
Setelah mengetahui hewan apa saja yang harus dijaga Grape-kun melakukan zip kepada direktori yang dia buat sebelumnya agar menghemat penyimpanan.

## Analisis soal:
melakukan zip kepada direktori yang dibuat 
## Kendala
library zip.h terjadi error dimana tidak dapat dijalankan 
## Cara penyelesaian soal 1d:
sesuai dengan catatan yang diberikan dalam melakukan zip dan unzip tidak boleh menggunakan sistem sehingga disini saya mengunakan library zip.h
## Source code 
```sh
    zip("HewanDarat.zip", "HewanDarat");
    zip("HewanAmphibi.zip", "HewanAmphibi");
    zip("HewanAir.zip", "HewanAir");

```
## Test output
[![HNTvWOv.png](https://iili.io/HNTvWOv.png)](https://freeimage.host/id)

## SOAL 3
Ten Hag adalah seorang pelatih Ajax di Liga Belanda. Suatu hari, Ten Hag mendapatkan tawaran untuk menjadi manajer Manchester United. Karena Ten Hag masih mempertimbangkan tawaran tersebut, ia ingin mengenal para pemain yang akan dilatih kedepannya. Dikarenakan Ten Hag hanya mendapatkan url atau link database mentah para pemain bola, maka ia perlu melakukan klasifikasi pemain Manchester United. Bantulah Ten Hag untuk mengenal para pemain Manchester United tersebut hanya dengan 1 Program C bernama “filter.c”

### Soal 3a

Pertama-tama, Program filter.c akan mengunduh file yang berisikan database para pemain bola. Kemudian dalam program yang sama diminta dapat melakukan extract “players.zip”. Lalu hapus file zip tersebut agar tidak memenuhi komputer Ten Hag.

### Analisis soal:

Pada soal nomor 3a diminta untuk mendownload file yang berisikan database pemain bola dan melakukan extract serta menghapus file zip tersebut

### Cara penyelesaian soal 1a:

Dalam penyelesaian dari soal ini adalah digunakan variabel char yang berisikan link menuju google drive yang berisikan database pemain bola tersebut, Kemudian dibuat sebuah function yang bertugas mendownload, meng-extract, dna juga menghapus folder tersebut. int file_download Fungsi ini digunakan untuk mengunduh file dari suatu URL dan menyimpannya dengan nama tertentu di direktori lokal. Fungsi ini menggunakan library libcurl untuk melakukan pengunduhan. int file_extract Fungsi ini digunakan untuk mengekstrak file yang telah diunduh sebelumnya ke suatu direktori tujuan. Fungsi ini menggunakan perintah unzip melalui fungsi execvp untuk melakukan ekstraksi file. 

### Kendala

Belum ada kendala

### Soal 3b

Dikarenakan database yang diunduh masih data mentah. Maka bantulah Ten Hag untuk menghapus semua pemain yang bukan dari Manchester United yang ada di directory.  

### Analisis soal:

Menghapus semua pemain yang bukan dari MU pada directory

### Cara penyelesaian soal 3b:

Solusi untuk poin b ini diharuskan membuat function yang berguna untuk manghapus pemain yang bukan dari MU yaitu rm_non_ManUtd()

### Kendala

Belum ada kendala

### Soal 3c

Setelah mengetahui nama-nama pemain Manchester United, Ten Hag perlu untuk mengkategorikan pemain tersebut sesuai dengan posisi mereka dalam waktu bersamaan dengan 4 proses yang berbeda. Untuk kategori folder akan menjadi 4 yaitu Kiper, Bek, Gelandang, dan Penyerang.

### Analisis soal:

Mengkategorikan pemain menjadi 4 yaitu Kiper, Bek, Gelandang, dan Penyerang

### Cara penyelesaian soal 3c:

Proses penyelesaiannya diharuskan menggunakan fork () dan execv () untuk membuat directory baru dan untuk menjalankan perintah find.

### Kendala

Kesulitan membuat perkondisian untuk mengkategorikan berdasarkan database yang telah difilter menggunakan yang bukan pemain MU

### Soal 3d

Setelah mengkategorikan anggota tim Manchester United, Ten Hag memerlukan Kesebelasan Terbaik untuk menjadi senjata utama MU berdasarkan rating terbaik dengan wajib adanya kiper, bek, gelandang, dan penyerang. (Kiper pasti satu pemain). Untuk output nya akan menjadi Formasi_[jumlah bek]-[jumlah gelandang]-[jumlah penyerang].txt dan akan ditaruh di /home/[users]/

### Analisis soal:

Membuat formasi team yang terbaik menurut rating pemain yang terbaik berdasarkan input user untuk formasinya

### Cara penyelesaian soal 3d:

Dalam penyelesaian dari soal ini adalah digunakan function untuk membandingkan rating rating pemain untuk mendapatkan pemain dengan rating terbaik dengan menggunakan function compare dan membuat tim berdasarkan formasi yang telah diinput oleh user sebelumnya dengan function createTeam

### Kendala

Sulit untuk membandingkan pemain berdasarkan rating

## Source code
- filter.c
```c
#include <stdio.h>
#include <stdlib.h>
#include <curl/curl.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <dirent.h>

#define URL "https://drive.google.com/u/0/uc?id=1zEAneJ1-0sOgt13R1gL4i1ONWfKAtwBF&export=download"
#define FILENAME "players.zip"

typedef struct
{
    char name[50];
    char team[50]; 
    char position[50];
    int rating;
} Player;

int file_download(char *url, char *filename);
int file_extract(const char *filename, const char *dest);
void rm_non_ManUtd(const char *dir_path);
void read_player_filenames(const char *directory, Player *players, int *rating);
int compare_files_players(const void *p1, const void *p2);
void sort_files_players(Player *players, int rating);
void createTeam();
void buatTim(int num_goalkeepers, int num_defenders, int num_midfielders, int num_strikers);

int main()
{

    if (!file_download(URL, FILENAME))
    {
        fprintf(stderr, "Failed to download file.\n");
        return 1;
    }

    printf("File downloaded successfully.\n");

    if (!file_extract(FILENAME, "players"))
    {
        fprintf(stderr, "Failed to extract file.\n");
        return 1;
    }

    printf("File extracted successfully.\n");

    rm_non_ManUtd("/home/yohanneslex/sisop/modul2/soal3/players/players");

    printf("Non-ManUtd files removed successfully.\n");

    if (remove(FILENAME) != 0)
    {
        fprintf(stderr, "Failed to remove file.\n");
        return 1;
    }

    printf("File removed successfully.\n");

    // buatTim(1, 4, 4, 2); // 1 kiper, 4 bek, 4 gelandang, 2 penyerang
    // call createTeam() function
    createTeam();
    return 0;
}

/* Function to download file from URL */
int file_download(char *url, char *filename)
{
    CURL *curl;
    FILE *fp;
    int success = 0;

    curl = curl_easy_init();
    if (curl)
    {
        fp = fopen(filename, "wb");
        curl_easy_setopt(curl, CURLOPT_URL, url);
        curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, fp);
        CURLcode res = curl_easy_perform(curl);
        if (res == CURLE_OK)
        {
            success = 1;
        }
        else
        {
            fprintf(stderr, "Error: %s\n", curl_easy_strerror(res));
        }
        fclose(fp);
        curl_easy_cleanup(curl);
    }

    return success;
}

/* Function to extract file to destination directory */
int file_extract(const char *filename, const char *dest)
{
    int status;
    pid_t pid = fork();

    if (pid == -1)
    {
        perror("Failed to create child process");
        return 0;
    }

    if (pid == 0)
    {
        char *unzip_args[] = {"unzip", "-oq", filename, "-d", dest, NULL};
        execvp("unzip", unzip_args);
        perror("Failed to execute unzip");
        exit(EXIT_FAILURE);
    }
    else
    {
        if (waitpid(pid, &status, 0) == -1)
        {
            perror("Failed to wait for child process");
            return 0;
        }
        if (!WIFEXITED(status) || WEXITSTATUS(status) != EXIT_SUCCESS)
        {
            return 0;
        }
    }

    return 1;
}

/* Function to remove non manutd*/
void rm_non_ManUtd(const char *dir_path)
{
    DIR *dir = opendir(dir_path);
    if (dir == NULL)
    {
        perror("Failed to open directory");
        return;
    }

    struct dirent *ent;
    while ((ent = readdir(dir)) != NULL)
    {
        char file_path[256];
        snprintf(file_path, sizeof(file_path), "%s/%s", dir_path, ent->d_name);

        struct stat st;
        if (stat(file_path, &st) == -1)
        {
            perror("Failed to get file info");
            continue;
        }

        if (S_ISREG(st.st_mode))
        {
            if (strstr(ent->d_name, "ManUtd") == NULL)
            {
                if (remove(file_path) == -1)
                {
                    perror("Failed to remove file");
                }
            }
        }
    }

    /* Function to categorize */

    int status;

    // Create the folder for "Kiper" players
    char *make_kiper[] = {"mkdir", "-p", "/home/yohanneslex/sisop/modul2/soal3/players/players/Kiper", NULL};
    pid_t pid = fork();
    if (pid == 0)
    {
        execvp(make_kiper[0], make_kiper);
        perror("execvp");
        exit(EXIT_FAILURE);
    }
    else if (pid < 0)
    {
        perror("fork");
        exit(EXIT_FAILURE);
    }
    else
    {
        waitpid(pid, &status, 0);
    }

    // move files containing "Kiper" to the "Kiper" folder
    char *find_kiper[] = {"find", "/home/yohanneslex/sisop/modul2/soal3/players/players", "-maxdepth", "1", "-type", "f", "-name", "*Kiper*", "-execdir", "mv", "{}", "./Kiper", ";", NULL};
    pid = fork();
    if (pid == 0)
    {
        execvp(find_kiper[0], find_kiper);
        perror("execvp");
        exit(EXIT_FAILURE);
    }
    else if (pid < 0)
    {
        perror("fork");
        exit(EXIT_FAILURE);
    }
    else
    {
        waitpid(pid, &status, 0);
    }

    // Create the folder for "Bek" players
    char *make_bek[] = {"mkdir", "-p", "/home/yohanneslex/sisop/modul2/soal3/players/players/Bek", NULL};
    pid_t pid2 = fork();
    if (pid2 == 0)
    {
        execvp(make_bek[0], make_bek);
        perror("execvp");
        exit(EXIT_FAILURE);
    }
    else if (pid2 < 0)
    {
        perror("fork");
        exit(EXIT_FAILURE);
    }
    else
    {
        waitpid(pid2, &status, 0);
    }

    // Move files containing "Bek" to the "Bek" folder
    char *find_bek[] = {"find", "/home/yohanneslex/sisop/modul2/soal3/players/players", "-maxdepth", "1", "-type", "f", "-name", "*Bek*", "-execdir", "mv", "{}", "./Bek", ";", NULL};
    pid2 = fork();
    if (pid2 == 0)
    {
        execvp(find_bek[0], find_bek);
        perror("execvp");
        exit(EXIT_FAILURE);
    }
    else if (pid2 < 0)
    {
        perror("fork");
        exit(EXIT_FAILURE);
    }
    else
    {
        waitpid(pid2, &status, 0);
    }

    // Create the folder for "Gelandang" players
    char *make_gelandang[] = {"mkdir", "-p", "/home/yohanneslex/sisop/modul2/soal3/players/players/Gelandang", NULL};
    pid_t pid3 = fork();
    if (pid3 == 0)
    {
        execvp(make_gelandang[0], make_gelandang);
        perror("execvp");
        exit(EXIT_FAILURE);
    }
    else if (pid3 < 0)
    {
        perror("fork");
        exit(EXIT_FAILURE);
    }
    else
    {
        waitpid(pid3, &status, 0);
    }

    // Move files containing "Gelandang" to the "Gelandang" folder
    char *find_gelandang[] = {"find", "/home/yohanneslex/sisop/modul2/soal3/players/players", "-maxdepth", "1", "-type", "f", "-name", "*Gelandang*", "-execdir", "mv", "{}", "./Gelandang", ";", NULL};
    pid3 = fork();
    if (pid3 == 0)
    {
        execvp(find_gelandang[0], find_gelandang);
        perror("execvp");
        exit(EXIT_FAILURE);
    }
    else if (pid3 < 0)
    {
        perror("fork");
        exit(EXIT_FAILURE);
    }
    else
    {
        waitpid(pid3, &status, 0);
    }

    // Create the folder for "Penyerang" players
    char *make_penyerang[] = {"mkdir", "-p", "/home/yohanneslex/sisop/modul2/soal3/players/players/Penyerang", NULL};
    pid_t pid4 = fork();
    if (pid4 == 0)
    {
        execvp(make_penyerang[0], make_penyerang);
        perror("execvp");
        exit(EXIT_FAILURE);
    }
    else if (pid4 < 0)
    {
        perror("fork");
        exit(EXIT_FAILURE);
    }
    else
    {
        waitpid(pid4, &status, 0);
    }

    // Move files containing "Penyerang" to the "Penyerang" folder
    char *find_penyerang[] = {"find", "/home/yohanneslex/sisop/modul2/soal3/players/players", "-maxdepth", "1", "-type", "f", "-name", "*Penyerang*", "-execdir", "mv", "{}", "./Penyerang", ";", NULL};
    pid4 = fork();
    if (pid4 == 0)
    {
        execvp(find_penyerang[0], find_penyerang);
        perror("execvp");
        exit(EXIT_FAILURE);
    }
    else if (pid4 < 0)
    {
        perror("fork");
        exit(EXIT_FAILURE);
    }
    else
    {
        waitpid(pid4, &status, 0);
    }

    // Check if any files have been moved to the their own folder
    {
        pid_t pid;
        int pipefd[2];
        int count = 0;
        char buffer[1024];

        char *find_args[] = {"find", "/home/yohanneslex/sisop/modul2/soal3/players/players/Kiper", "/home/yohanneslex/sisop/modul2/soal3/players/players/Bek", "/home/yohanneslex/sisop/modul2/soal3/players/players/Gelandang/", "/home/yohanneslex/sisop/modul2/soal3/players/players/Penyerang/", "-maxdepth", "1", "-type", "f", "-printf", ";", NULL};

        if (pipe(pipefd) < 0)
        {
            perror("pipe");
            exit(EXIT_FAILURE);
        }

        pid = fork();
        if (pid < 0)
        {
            perror("fork");
            exit(EXIT_FAILURE);
        }
        else if (pid == 0)
        {
            close(pipefd[0]); // Close unused read end

            // Redirect stdout to the write end of the pipe
            dup2(pipefd[1], STDOUT_FILENO);

            // Close original write end of the pipe
            close(pipefd[1]);

            // Execute find command
            execvp(find_args[0], find_args);
            perror("execvp");
            exit(EXIT_FAILURE);
        }

        close(pipefd[1]); // Close unused write end

        // Read output of find command from read end of the pipe
        while (read(pipefd[0], buffer, sizeof(buffer)) != 0)
        {
            if (strstr(buffer, "Kiper") || strstr(buffer, "Bek") || strstr(buffer, "Gelandang") || strstr(buffer, "Penyerang"))
            {
                count++;
            }
        }

        // Wait for child process to finish
        waitpid(pid, NULL, 0);

        if (count == 0)
        {
            printf("No files containing 'Kiper', 'Bek', 'Gelandang', 'Penyerang' were moved.\n");
        }
        else
        {
            printf("Fles containing 'Kiper', 'Bek', 'Gelandang', 'Penyerang' were moved.\n");
        }

        return 0;
    }
}

// Function to read file names
void read_player_filenames(const char *directory, Player *players, int *num_players)
{
    DIR *dir = opendir(directory);
    if (dir == NULL)
    {
        printf("Error opening directory %s\n", directory);
        return;
    }
    struct dirent *ent;
    while ((ent = readdir(dir)) != NULL)
    {
        // Check if the file is a .png file with the correct format
        char *ext = strrchr(ent->d_name, '.');
        if (ext != NULL && strcmp(ext, ".png") == 0)
        {
            // Split the file name by the underscore character
            char *name = strtok(ent->d_name, "_");
            char *team = strtok(NULL, "_");
            char *position = strtok(NULL, "_");
            char *rating_str = strtok(NULL, "_");
            if (name != NULL && team != NULL && position != NULL && rating_str != NULL)
            {
                // Convert the rating from a string to an integer
                int rating = atoi(rating_str);
                printf("name = %s, team = %s, position = %s, rating = %d\n", name, team, position, rating);

                // Add the player to the array of players
                Player player;
                strcpy(player.name, name);
                strcpy(player.team, team);
                strcpy(player.position, position);
                player.rating = rating;
                players[*num_players] = player;
                (*num_players)++;
            }
            else
            {
                printf("Error parsing player information from file name: %s\n", ent->d_name);
            }
        }
    }
}

// Compare function for sorting players by position and rating
int compare_files_players(const void *p1, const void *p2)
{
    Player *player1 = (Player *)p1;
    Player *player2 = (Player *)p2;

    // compare ratings
    return player2->rating - player1->rating;
}

// Sort players by position and rating using the quicksort algorithm
void sort_files_players(Player *players, int rating)
{
    qsort(players, rating, sizeof(Player), compare_files_players);
}

// Input the formation number
void createTeam()
{
    int num_defenders, num_midfielders, num_strikers;

    printf("Enter the number of defenders: ");
    scanf("%d", &num_defenders);

    printf("Enter the number of midfielders: ");
    scanf("%d", &num_midfielders);

    printf("Enter the number of strikers: ");
    scanf("%d", &num_strikers);

    buatTim(1, num_defenders, num_midfielders, num_strikers);
}

// Function to make the best 11 player team
void buatTim(int num_goalkeepers, int num_defenders, int num_midfielders, int num_strikers)
{
    Player players[100];
    int rating = 0;

    // Read player information from each directory
    read_player_filenames("/home/yohanneslex/sisop/modul2/soal3/players/players/Kiper", players, &rating);
    read_player_filenames("/home/yohanneslex/sisop/modul2/soal3/players/players/Bek", players, &rating);
    read_player_filenames("/home/yohanneslex/sisop/modul2/soal3/players/players/Gelandang", players, &rating);
    read_player_filenames("/home/yohanneslex/sisop/modul2/soal3/players/players/Penyerang", players, &rating);

    // Sort players by rating
    sort_files_players(players, rating);

    // Assign players to positions
    int num_players = rating;
    int num_kiper_terpilih = 0;
    int num_bek_terpilih = 0;
    int num_gelandang_terpilih = 0;
    int num_striker_terpilih = 0;

    // Assign Kiper
    printf("Kiper:\n");
    for (int i = 0; i < rating && num_kiper_terpilih < num_goalkeepers; i++)
    {
        if (strcmp(players[i].position, "Kiper") == 0)
        {
            printf("Assigned %s (rating: %d)\n", players[i].name, players[i].rating);
            num_kiper_terpilih++;
        }
    }

    // Assign Bek
    printf("\nBek:\n");
    for (int i = 0; i < rating && num_bek_terpilih < num_defenders; i++)
    {
        if (strcmp(players[i].position, "Bek") == 0)
        {
            printf("Assigned %s (rating: %d)\n", players[i].name, players[i].rating);
            num_bek_terpilih++;
        }
    }

    // Assign Gelandang
    printf("\nGelandang:\n");
    for (int i = 0; i < rating && num_gelandang_terpilih < num_midfielders; i++)
    {
        if (strcmp(players[i].position, "Gelandang") == 0)
        {
            printf("Assigned %s (rating: %d)\n", players[i].name, players[i].rating);
            num_gelandang_terpilih++;
        }
    }

    // Assign Penyerang
    printf("\nPenyerang:\n");
    for (int i = 0; i < rating && num_striker_terpilih < num_strikers; i++)
    {
        if (strcmp(players[i].position, "Penyerang") == 0)
        {
            printf("Assigned %s (rating: %d)\n", players[i].name, players[i].rating);
            num_striker_terpilih++;
        }
    }

    // Write output to file
    char filename[50];
    sprintf(filename, "Formasi_%d-%d-%d.txt", num_defenders, num_midfielders, num_strikers);
    FILE *txt_file_output = fopen(filename, "w");

    if (txt_file_output == NULL)
    {
        printf("Error opening output file\n");
        return;
    }

    printf("File opened successfully\n");

    // Write assigned players to file
    fprintf(txt_file_output, "Assigned players:\n");

    // Print KIPER
    fprintf(txt_file_output, "\nKiper:\n");
    num_kiper_terpilih = 0;
    for (int i = 0; i < rating; i++)
    {
        if (strcmp(players[i].position, "Kiper") == 0 && num_kiper_terpilih < num_goalkeepers)
        {
            fprintf(txt_file_output, "%s_%s_%s_%d.png\n", players[i].name, players[i].team, players[i].position, players[i].rating);
            num_kiper_terpilih++;
        }
    }

    // Print BEK
    fprintf(txt_file_output, "\nBek:\n");
    num_bek_terpilih = 0;
    for (int i = 0; i < rating; i++)
    {
        if (strcmp(players[i].position, "Bek") == 0 && num_bek_terpilih < num_defenders)
        {
            fprintf(txt_file_output, "%s_%s_%s_%d.png\n", players[i].name, players[i].team, players[i].position, players[i].rating);
            num_bek_terpilih++;
        }
    }

    // Print GELANDANG
    fprintf(txt_file_output, "\nGelandang:\n");
    num_gelandang_terpilih = 0;
    for (int i = 0; i < rating; i++)
    {
        if (strcmp(players[i].position, "Gelandang") == 0 && num_gelandang_terpilih < num_midfielders)
        {
            fprintf(txt_file_output, "%s_%s_%s_%d.png\n", players[i].name, players[i].team, players[i].position, players[i].rating);
            num_gelandang_terpilih++;
        }
    }

    // Print PENYERANG
    fprintf(txt_file_output, "\nPenyerang:\n");
    num_striker_terpilih = 0;
    for (int i = 0; i < rating; i++)
    {
        if (strcmp(players[i].position, "Penyerang") == 0 && num_striker_terpilih < num_strikers)
        {
            fprintf(txt_file_output, "%s_%s_%s_%d.png\n", players[i].name, players[i].team, players[i].position, players[i].rating);
            num_striker_terpilih++;
        }
    }

    // Close output file
    fclose(txt_file_output);

    printf("File closed successfully\n");
}

```

Jalankan compile program tersebut dan jalankan
```
gcc -o filter filter.c -lcurl

./filter
```

## Test output
Menjalankan Program
[![image](https://www.linkpicture.com/q/output1_1.png)](https://www.linkpicture.com/view.php?img=LPic64305a7051f761923023291)

Output Pada Program
[![image](https://www.linkpicture.com/q/output2.png)](https://www.linkpicture.com/view.php?img=LPic64305a7051f761923023291)

Kategori Pemain
[![image](https://www.linkpicture.com/q/output3.png)](https://www.linkpicture.com/view.php?img=LPic64305a7051f761923023291)

Kategori Bek
[![image](https://www.linkpicture.com/q/output4.png)](https://www.linkpicture.com/view.php?img=LPic64305a7051f761923023291)

Kategori Gelandang
[![image](https://www.linkpicture.com/q/output5.png)](https://www.linkpicture.com/view.php?img=LPic64305a7051f761923023291)

Kategori Kiper
[![image](https://www.linkpicture.com/q/output6.png)](https://www.linkpicture.com/view.php?img=LPic64305a7051f761923023291)

Kategori Penyerang
[![image](https://www.linkpicture.com/q/output7.png)](https://www.linkpicture.com/view.php?img=LPic64305a7051f761923023291)

Output Final
[![image](https://www.linkpicture.com/q/output8.png)](https://www.linkpicture.com/view.php?img=LPic64305a7051f761923023291)

## Revisi Nomor 3
Menambahkan poin d ke dalam program dikarenakan yang dikumpulkan sebelumnya hanya mencakup poin a, b, dan c

## SOAL 4
## Soal 4
Banabil harus melatih diri sendiri dalam membuat program untuk menjalankan script bash yang menyerupai crontab dan menggunakan bahasa C karena baru dipelajari olehnya. Dengan beberapa ketentuan :

- Tidak menggunakan fungsi system()
- Program dapat menerima argumen berupa Jam (0-23), Menit (0-59), Detik (0-59), Tanda asterisk [ * ] (value bebas), serta path file .sh.
- Program dapat mengeluarkan pesan “error” apabila argumen yang diterima program tidak sesuai. Pesan error dapat dibentuk sesuka hati oleh pembuat program. terserah bagaimana, yang penting tulisan error.
- Program ini berjalan dalam background dan hanya menerima satu config cron.

## Analisis soal
membuat sebuah program.c untuk mengeksekusi script bash dengan menangkap argumen berupa jam, menit, detik, file_path.sh dengan tambahan apabila diinputkan tanda [ * ] pada jam atau menit atau detik yang berarti bahwa value nya bebas. Selain itu juga menampilkan pesan error jika argumen yang diinputkan tidak sesuai dengan ketentuan soal dan juga program ini berjalan dilatarbelakang dengan hanya menerima satu config cron.

## Kendala
Tidak terdapat kendala pada pengerjaan soal ini

## Cara penyelesaian soal 4 :
- menggunakan library sebagai berikut :
```
    #include <stdio.h>   //untuk input dan output standar
    #include <stdlib.h>   //untuk fungsi-fungsi standar dalam bahasa C
    #include <unistd.h>   //untuk fungsi-fungsi yang terkait dengan sistem operasi seperti fork(), sleep(), exit(), dll.
    #include <string.h>   //untuk fungsi-fungsi yang terkait dengan string seperti strcmp(), strcpy(), strtok(), dll.
    #include <time.h>   //untuk fungsi-fungsi yang terkait dengan waktu seperti time(), localtime(), difftime(), dll.
    #include <signal.h>   //untuk fungsi-fungsi yang terkait dengan penanganan sinyal seperti signal(), SIGTERM, SIGINT, dll.
```

- membuat inisialisasi tipe data dan variabel untuk menyimpan masing-masing argumen
```sh
    int jam, menit, detik;
    char *file_path; // yang dimaksud file_path disini adalah alamat file script bash yang akan dieksekusi
    char *args[] = {jam, menit, detik, file_path};
```
- mengecek jumlah argumen yang diterima oleh program. Jika jumlahnya tidak sama dengan 5, maka program akan menampilkan pesan penggunaan yang benar serta keluar dari program dengan status 1 menggunakan fungsi exit().
```sh
    if (argc != 5) {
        printf("Usage: %s <jam> <menit> <detik> <file_path>\n", argv[0]); // argv[0] adalah string yang berisi nama program itu sendiri.
        exit(1);
    }
```

- memeriksa apakah argumen pertama yang diberikan pada program (argv[1]) bernilai "*", yang berarti program harus dijalankan setiap jam. Jika ya, maka variabel jam di-set menjadi -1. Namun, jika tidak, maka nilai argv[1] diubah menjadi integer menggunakan fungsi atoi(), dan kemudian diperiksa apakah nilainya berada di antara 0 dan 23 (karena jam dalam format 24 jam hanya memiliki rentang nilai dari 0 hingga 23). Jika nilainya tidak sesuai, maka program akan menampilkan pesan kesalahan dan keluar.
```sh
    if (strcmp(argv[1], "*") == 0) {
        jam = -1;
    } else {
        jam = atoi(argv[1]);
        if (jam < 0 || jam > 23) {
            printf("Error: jam harus bernilai antara 0 hingga 23\n");
            exit(1);
        }
    }
```

- melakukan pengecekan terhadap nilai kedua yang dimasukkan pada saat program dijalankan. Jika nilai tersebut sama dengan [ * ] maka nilai variabel "menit" akan diisi dengan -1, yang artinya tidak ada batasan pada nilai menit (setiap menit). Namun jika nilai tersebut bukan [ * ] maka program akan mengonversi nilai tersebut menjadi integer menggunakan fungsi atoi(argv[2]), lalu melakukan pengecekan apakah nilainya berada dalam rentang 0-59. Jika nilai menit diluar rentang tersebut, maka program akan mencetak pesan error dan keluar dari program dengan exit code 1.
```sh
    if (strcmp(argv[2], "*") == 0) {
        menit = -1;
    } else {
        menit = atoi(argv[2]);
        if (menit < 0 || menit > 59) {
            printf("Error: menit harus bernilai antara 0 hingga 59\n");
            exit(1);
        }
    }
```

- memeriksa apakah argumen ketiga yang diberikan (detik) sama dengan tanda [ * ] atau tidak. Jika sama dengan [ * ], maka variabel detik diatur menjadi -1, yang menunjukkan bahwa interval waktu tidak memperhatikan detik keberapa yang berarti setiap detik. Jika argumen detik bukan "*", maka argumen tersebut diubah menjadi integer menggunakan fungsi atoi() dan kemudian diperiksa apakah nilainya berada dalam rentang 0-59. Jika nilainya tidak dalam rentang tersebut, maka program akan menampilkan pesan kesalahan dan keluar menggunakan fungsi exit().
```sh
    if (strcmp(argv[3], "*") == 0) {
        detik = -1;
    } else {
        detik = atoi(argv[3]);
        if (detik < 0 || detik > 59) {
            printf("Error: detik harus bernilai antara 0 hingga 59\n");
            exit(1);
        }
    }
```

- menyimpan alamat file_path dari script bash
```sh
file_path = argv[4];
```

- membuat program berjalan dilatarbelakang dengan menggunakan fungsi fork() dan menangkan nilai kembalian fork() kedalam variabel pid_t pid_t
```sh
pid_t pid = fork();
```

- melakukan fork untuk membuat child process agar dapat berjalan di background. Jika fork gagal, program akan keluar dengan exit code 1. Jika berhasil dilakukan fork, parent process akan langsung keluar dengan exit code 0, sehingga child process akan berjalan secara mandiri.
```sh
    if (pid == -1) {
        printf("Error: Failed to fork process\n");
        exit(1);
    } else if (pid > 0) {
        exit(0);
    }
```

- melakukan set signal handler untuk sinyal SIGTERM, sehingga jika program menerima sinyal tersebut, maka program akan keluar dengan exit code 0.
```sh
signal(SIGTERM, exit);
```

- melakukan loop tanpa henti yang memeriksa waktu saat ini setiap detiknya. Jika waktu saat ini sama dengan waktu yang telah ditentukan di argument program, maka program akan menjalankan script bash dengan memanggil fungsi execv. Jika execv mengembalikan nilai selain 0, maka program akan menampilkan pesan error. Program juga akan melakukan sleep selama 1 detik untuk menunda pengecekan waktu selanjutnya.
```sh
    while (1) {
        time_t now = time(NULL);
        struct tm *tm = localtime(&now);

        if ((jam == -1 || jam == tm->tm_hour) &&
            (menit == -1 || menit == tm->tm_min) &&
            (detik == -1 || detik == tm->tm_sec)) {
            int result = execv(file_path, args);
            if (result != 0) {
                printf("Error: Failed to run the script\n");
            }
        }
        sleep(1);
    }
```

## Source code
- mainan.c
```
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include <signal.h>

int main(int argc, char *argv[]) {
    int jam, menit, detik;
    char *file_path;
    char *args[] = {jam, menit, detik, file_path};

    if (argc != 5) {
        printf("Usage: %s <jam> <menit> <detik> <file_path>\n", argv[0]);
        exit(1);
    }

    if (strcmp(argv[1], "*") == 0) {
        jam = -1;
    } else {
        jam = atoi(argv[1]);
        if (jam < 0 || jam > 23) {
            printf("Error: jam harus bernilai antara 0 hingga 23\n");
            exit(1);
        }
    }

    if (strcmp(argv[2], "*") == 0) {
        menit = -1;
    } else {
        menit = atoi(argv[2]);
        if (menit < 0 || menit > 59) {
            printf("Error: menit harus bernilai antara 0 hingga 59\n");
            exit(1);
        }
    }

    if (strcmp(argv[3], "*") == 0) {
        detik = -1;
    } else {
        detik = atoi(argv[3]);
        if (detik < 0 || detik > 59) {
            printf("Error: detik harus bernilai antara 0 hingga 59\n");
            exit(1);
        }
    }

    file_path = argv[4];
    pid_t pid = fork();

    if (pid == -1) {
        printf("Error: Failed to fork process\n");
        exit(1);
    } else if (pid > 0) {
        exit(0);
    }

    signal(SIGTERM, exit);

    while (1) {
        time_t now = time(NULL);
        struct tm *tm = localtime(&now);

        if ((jam == -1 || jam == tm->tm_hour) &&
            (menit == -1 || menit == tm->tm_min) &&
            (detik == -1 || detik == tm->tm_sec)) {
            int result = execv(file_path, args);
            if (result != 0) {
                printf("Error: Failed to run the script\n");
            }
        }
        sleep(1);
    }

    return 0;
}
```

- programcron.sh
```
#!/bin/bash

echo "Running programcron.sh at $(date)"
```

## Test output
- ketika argumen diiunputkan dengan benar dan ingin dijalankan setiap detik
![](https://i.imgur.com/RJmp1ni.png)

- ketika argumen diiunputkan dengan benar dan ingin dijalankan spesifik pada pukul 16:09:30
![](https://i.imgur.com/a3nJZ3F.png)

- ketika salah menginput argumen dengan tidak memberikan alamat file script 
![](https://i.imgur.com/EKnOuq1.png)

- ketika salah menginput argumen dengan mengosongkan detik
![](https://i.imgur.com/QY6mugg.png)

- ketika salah menginputkan nilai detik yakni "60"
![](https://i.imgur.com/9iwXkl3.png)

- ketika salah menginputkan nilai menit yakni "61"
![](https://i.imgur.com/3vcFL40.png)

- ketika salah menginputkan nilai jam yakni "25"
![](https://i.imgur.com/OSGXCu4.png)