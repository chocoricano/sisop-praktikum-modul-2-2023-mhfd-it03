//terealisasikan poin a, b, dan c
//revisi untuk menambahkan poin d

#include <stdio.h>
#include <stdlib.h>
#include <curl/curl.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <dirent.h>

#define URL "https://drive.google.com/u/0/uc?id=1zEAneJ1-0sOgt13R1gL4i1ONWfKAtwBF&export=download"
#define FILENAME "players.zip"

typedef struct
{
    char name[50];
    char team[50]; 
    char position[50];
    int rating;
} Player;

int file_download(char *url, char *filename);
int file_extract(const char *filename, const char *dest);
void rm_non_ManUtd(const char *dir_path);
void read_player_filenames(const char *directory, Player *players, int *rating);
int compare_files_players(const void *p1, const void *p2);
void sort_files_players(Player *players, int rating);
void createTeam();
void buatTim(int num_goalkeepers, int num_defenders, int num_midfielders, int num_strikers);

int main()
{

    if (!file_download(URL, FILENAME))
    {
        fprintf(stderr, "Failed to download file.\n");
        return 1;
    }

    printf("File downloaded successfully.\n");

    if (!file_extract(FILENAME, "players"))
    {
        fprintf(stderr, "Failed to extract file.\n");
        return 1;
    }

    printf("File extracted successfully.\n");

    rm_non_ManUtd("/home/yohanneslex/sisop/modul2/soal3/players/players");

    printf("Non-ManUtd files removed successfully.\n");

    if (remove(FILENAME) != 0)
    {
        fprintf(stderr, "Failed to remove file.\n");
        return 1;
    }

    printf("File removed successfully.\n");

    // buatTim(1, 4, 4, 2); // 1 kiper, 4 bek, 4 gelandang, 2 penyerang
    // call createTeam() function
    createTeam();
    return 0;
}

/* Function to download file from URL */
int file_download(char *url, char *filename)
{
    CURL *curl;
    FILE *fp;
    int success = 0;

    curl = curl_easy_init();
    if (curl)
    {
        fp = fopen(filename, "wb");
        curl_easy_setopt(curl, CURLOPT_URL, url);
        curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, fp);
        CURLcode res = curl_easy_perform(curl);
        if (res == CURLE_OK)
        {
            success = 1;
        }
        else
        {
            fprintf(stderr, "Error: %s\n", curl_easy_strerror(res));
        }
        fclose(fp);
        curl_easy_cleanup(curl);
    }

    return success;
}

/* Function to extract file to destination directory */
int file_extract(const char *filename, const char *dest)
{
    int status;
    pid_t pid = fork();

    if (pid == -1)
    {
        perror("Failed to create child process");
        return 0;
    }

    if (pid == 0)
    {
        char *unzip_args[] = {"unzip", "-oq", filename, "-d", dest, NULL};
        execvp("unzip", unzip_args);
        perror("Failed to execute unzip");
        exit(EXIT_FAILURE);
    }
    else
    {
        if (waitpid(pid, &status, 0) == -1)
        {
            perror("Failed to wait for child process");
            return 0;
        }
        if (!WIFEXITED(status) || WEXITSTATUS(status) != EXIT_SUCCESS)
        {
            return 0;
        }
    }

    return 1;
}

/* Function to remove non manutd*/
void rm_non_ManUtd(const char *dir_path)
{
    DIR *dir = opendir(dir_path);
    if (dir == NULL)
    {
        perror("Failed to open directory");
        return;
    }

    struct dirent *ent;
    while ((ent = readdir(dir)) != NULL)
    {
        char file_path[256];
        snprintf(file_path, sizeof(file_path), "%s/%s", dir_path, ent->d_name);

        struct stat st;
        if (stat(file_path, &st) == -1)
        {
            perror("Failed to get file info");
            continue;
        }

        if (S_ISREG(st.st_mode))
        {
            if (strstr(ent->d_name, "ManUtd") == NULL)
            {
                if (remove(file_path) == -1)
                {
                    perror("Failed to remove file");
                }
            }
        }
    }

    /* Function to categorize */

    int status;

    // Create the folder for "Kiper" players
    char *make_kiper[] = {"mkdir", "-p", "/home/yohanneslex/sisop/modul2/soal3/players/players/Kiper", NULL};
    pid_t pid = fork();
    if (pid == 0)
    {
        execvp(make_kiper[0], make_kiper);
        perror("execvp");
        exit(EXIT_FAILURE);
    }
    else if (pid < 0)
    {
        perror("fork");
        exit(EXIT_FAILURE);
    }
    else
    {
        waitpid(pid, &status, 0);
    }

    // move files containing "Kiper" to the "Kiper" folder
    char *find_kiper[] = {"find", "/home/yohanneslex/sisop/modul2/soal3/players/players", "-maxdepth", "1", "-type", "f", "-name", "*Kiper*", "-execdir", "mv", "{}", "./Kiper", ";", NULL};
    pid = fork();
    if (pid == 0)
    {
        execvp(find_kiper[0], find_kiper);
        perror("execvp");
        exit(EXIT_FAILURE);
    }
    else if (pid < 0)
    {
        perror("fork");
        exit(EXIT_FAILURE);
    }
    else
    {
        waitpid(pid, &status, 0);
    }

    // Create the folder for "Bek" players
    char *make_bek[] = {"mkdir", "-p", "/home/yohanneslex/sisop/modul2/soal3/players/players/Bek", NULL};
    pid_t pid2 = fork();
    if (pid2 == 0)
    {
        execvp(make_bek[0], make_bek);
        perror("execvp");
        exit(EXIT_FAILURE);
    }
    else if (pid2 < 0)
    {
        perror("fork");
        exit(EXIT_FAILURE);
    }
    else
    {
        waitpid(pid2, &status, 0);
    }

    // Move files containing "Bek" to the "Bek" folder
    char *find_bek[] = {"find", "/home/yohanneslex/sisop/modul2/soal3/players/players", "-maxdepth", "1", "-type", "f", "-name", "*Bek*", "-execdir", "mv", "{}", "./Bek", ";", NULL};
    pid2 = fork();
    if (pid2 == 0)
    {
        execvp(find_bek[0], find_bek);
        perror("execvp");
        exit(EXIT_FAILURE);
    }
    else if (pid2 < 0)
    {
        perror("fork");
        exit(EXIT_FAILURE);
    }
    else
    {
        waitpid(pid2, &status, 0);
    }

    // Create the folder for "Gelandang" players
    char *make_gelandang[] = {"mkdir", "-p", "/home/yohanneslex/sisop/modul2/soal3/players/players/Gelandang", NULL};
    pid_t pid3 = fork();
    if (pid3 == 0)
    {
        execvp(make_gelandang[0], make_gelandang);
        perror("execvp");
        exit(EXIT_FAILURE);
    }
    else if (pid3 < 0)
    {
        perror("fork");
        exit(EXIT_FAILURE);
    }
    else
    {
        waitpid(pid3, &status, 0);
    }

    // Move files containing "Gelandang" to the "Gelandang" folder
    char *find_gelandang[] = {"find", "/home/yohanneslex/sisop/modul2/soal3/players/players", "-maxdepth", "1", "-type", "f", "-name", "*Gelandang*", "-execdir", "mv", "{}", "./Gelandang", ";", NULL};
    pid3 = fork();
    if (pid3 == 0)
    {
        execvp(find_gelandang[0], find_gelandang);
        perror("execvp");
        exit(EXIT_FAILURE);
    }
    else if (pid3 < 0)
    {
        perror("fork");
        exit(EXIT_FAILURE);
    }
    else
    {
        waitpid(pid3, &status, 0);
    }

    // Create the folder for "Penyerang" players
    char *make_penyerang[] = {"mkdir", "-p", "/home/yohanneslex/sisop/modul2/soal3/players/players/Penyerang", NULL};
    pid_t pid4 = fork();
    if (pid4 == 0)
    {
        execvp(make_penyerang[0], make_penyerang);
        perror("execvp");
        exit(EXIT_FAILURE);
    }
    else if (pid4 < 0)
    {
        perror("fork");
        exit(EXIT_FAILURE);
    }
    else
    {
        waitpid(pid4, &status, 0);
    }

    // Move files containing "Penyerang" to the "Penyerang" folder
    char *find_penyerang[] = {"find", "/home/yohanneslex/sisop/modul2/soal3/players/players", "-maxdepth", "1", "-type", "f", "-name", "*Penyerang*", "-execdir", "mv", "{}", "./Penyerang", ";", NULL};
    pid4 = fork();
    if (pid4 == 0)
    {
        execvp(find_penyerang[0], find_penyerang);
        perror("execvp");
        exit(EXIT_FAILURE);
    }
    else if (pid4 < 0)
    {
        perror("fork");
        exit(EXIT_FAILURE);
    }
    else
    {
        waitpid(pid4, &status, 0);
    }

    // Check if any files have been moved to the their own folder
    {
        pid_t pid;
        int pipefd[2];
        int count = 0;
        char buffer[1024];

        char *find_args[] = {"find", "/home/yohanneslex/sisop/modul2/soal3/players/players/Kiper", "/home/yohanneslex/sisop/modul2/soal3/players/players/Bek", "/home/yohanneslex/sisop/modul2/soal3/players/players/Gelandang/", "/home/yohanneslex/sisop/modul2/soal3/players/players/Penyerang/", "-maxdepth", "1", "-type", "f", "-printf", ";", NULL};

        if (pipe(pipefd) < 0)
        {
            perror("pipe");
            exit(EXIT_FAILURE);
        }

        pid = fork();
        if (pid < 0)
        {
            perror("fork");
            exit(EXIT_FAILURE);
        }
        else if (pid == 0)
        {
            close(pipefd[0]); // Close unused read end

            // Redirect stdout to the write end of the pipe
            dup2(pipefd[1], STDOUT_FILENO);

            // Close original write end of the pipe
            close(pipefd[1]);

            // Execute find command
            execvp(find_args[0], find_args);
            perror("execvp");
            exit(EXIT_FAILURE);
        }

        close(pipefd[1]); // Close unused write end

        // Read output of find command from read end of the pipe
        while (read(pipefd[0], buffer, sizeof(buffer)) != 0)
        {
            if (strstr(buffer, "Kiper") || strstr(buffer, "Bek") || strstr(buffer, "Gelandang") || strstr(buffer, "Penyerang"))
            {
                count++;
            }
        }

        // Wait for child process to finish
        waitpid(pid, NULL, 0);

        if (count == 0)
        {
            printf("No files containing 'Kiper', 'Bek', 'Gelandang', 'Penyerang' were moved.\n");
        }
        else
        {
            printf("Fles containing 'Kiper', 'Bek', 'Gelandang', 'Penyerang' were moved.\n");
        }

        return 0;
    }
}

// Function to read file names
void read_player_filenames(const char *directory, Player *players, int *num_players)
{
    DIR *dir = opendir(directory);
    if (dir == NULL)
    {
        printf("Error opening directory %s\n", directory);
        return;
    }
    struct dirent *ent;
    while ((ent = readdir(dir)) != NULL)
    {
        // Check if the file is a .png file with the correct format
        char *ext = strrchr(ent->d_name, '.');
        if (ext != NULL && strcmp(ext, ".png") == 0)
        {
            // Split the file name by the underscore character
            char *name = strtok(ent->d_name, "_");
            char *team = strtok(NULL, "_");
            char *position = strtok(NULL, "_");
            char *rating_str = strtok(NULL, "_");
            if (name != NULL && team != NULL && position != NULL && rating_str != NULL)
            {
                // Convert the rating from a string to an integer
                int rating = atoi(rating_str);
                printf("name = %s, team = %s, position = %s, rating = %d\n", name, team, position, rating);

                // Add the player to the array of players
                Player player;
                strcpy(player.name, name);
                strcpy(player.team, team);
                strcpy(player.position, position);
                player.rating = rating;
                players[*num_players] = player;
                (*num_players)++;
            }
            else
            {
                printf("Error parsing player information from file name: %s\n", ent->d_name);
            }
        }
    }
}

// Compare function for sorting players by position and rating
int compare_files_players(const void *p1, const void *p2)
{
    Player *player1 = (Player *)p1;
    Player *player2 = (Player *)p2;

    // compare ratings
    return player2->rating - player1->rating;
}

// Sort players by position and rating using the quicksort algorithm
void sort_files_players(Player *players, int rating)
{
    qsort(players, rating, sizeof(Player), compare_files_players);
}

// Input the formation number
void createTeam()
{
    int num_defenders, num_midfielders, num_strikers;

    printf("Enter the number of defenders: ");
    scanf("%d", &num_defenders);

    printf("Enter the number of midfielders: ");
    scanf("%d", &num_midfielders);

    printf("Enter the number of strikers: ");
    scanf("%d", &num_strikers);

    buatTim(1, num_defenders, num_midfielders, num_strikers);
}

// Function to make the best 11 player team
void buatTim(int num_goalkeepers, int num_defenders, int num_midfielders, int num_strikers)
{
    Player players[100];
    int rating = 0;

    // Read player information from each directory
    read_player_filenames("/home/yohanneslex/sisop/modul2/soal3/players/players/Kiper", players, &rating);
    read_player_filenames("/home/yohanneslex/sisop/modul2/soal3/players/players/Bek", players, &rating);
    read_player_filenames("/home/yohanneslex/sisop/modul2/soal3/players/players/Gelandang", players, &rating);
    read_player_filenames("/home/yohanneslex/sisop/modul2/soal3/players/players/Penyerang", players, &rating);

    // Sort players by rating
    sort_files_players(players, rating);

    // Assign players to positions
    int num_players = rating;
    int num_kiper_terpilih = 0;
    int num_bek_terpilih = 0;
    int num_gelandang_terpilih = 0;
    int num_striker_terpilih = 0;

    // Assign Kiper
    printf("Kiper:\n");
    for (int i = 0; i < rating && num_kiper_terpilih < num_goalkeepers; i++)
    {
        if (strcmp(players[i].position, "Kiper") == 0)
        {
            printf("Assigned %s (rating: %d)\n", players[i].name, players[i].rating);
            num_kiper_terpilih++;
        }
    }

    // Assign Bek
    printf("\nBek:\n");
    for (int i = 0; i < rating && num_bek_terpilih < num_defenders; i++)
    {
        if (strcmp(players[i].position, "Bek") == 0)
        {
            printf("Assigned %s (rating: %d)\n", players[i].name, players[i].rating);
            num_bek_terpilih++;
        }
    }

    // Assign Gelandang
    printf("\nGelandang:\n");
    for (int i = 0; i < rating && num_gelandang_terpilih < num_midfielders; i++)
    {
        if (strcmp(players[i].position, "Gelandang") == 0)
        {
            printf("Assigned %s (rating: %d)\n", players[i].name, players[i].rating);
            num_gelandang_terpilih++;
        }
    }

    // Assign Penyerang
    printf("\nPenyerang:\n");
    for (int i = 0; i < rating && num_striker_terpilih < num_strikers; i++)
    {
        if (strcmp(players[i].position, "Penyerang") == 0)
        {
            printf("Assigned %s (rating: %d)\n", players[i].name, players[i].rating);
            num_striker_terpilih++;
        }
    }

    // Write output to file
    char filename[50];
    sprintf(filename, "Formasi_%d-%d-%d.txt", num_defenders, num_midfielders, num_strikers);
    FILE *txt_file_output = fopen(filename, "w");

    if (txt_file_output == NULL)
    {
        printf("Error opening output file\n");
        return;
    }

    printf("File opened successfully\n");

    // Write assigned players to file
    fprintf(txt_file_output, "Assigned players:\n");

    // Print KIPER
    fprintf(txt_file_output, "\nKiper:\n");
    num_kiper_terpilih = 0;
    for (int i = 0; i < rating; i++)
    {
        if (strcmp(players[i].position, "Kiper") == 0 && num_kiper_terpilih < num_goalkeepers)
        {
            fprintf(txt_file_output, "%s_%s_%s_%d.png\n", players[i].name, players[i].team, players[i].position, players[i].rating);
            num_kiper_terpilih++;
        }
    }

    // Print BEK
    fprintf(txt_file_output, "\nBek:\n");
    num_bek_terpilih = 0;
    for (int i = 0; i < rating; i++)
    {
        if (strcmp(players[i].position, "Bek") == 0 && num_bek_terpilih < num_defenders)
        {
            fprintf(txt_file_output, "%s_%s_%s_%d.png\n", players[i].name, players[i].team, players[i].position, players[i].rating);
            num_bek_terpilih++;
        }
    }

    // Print GELANDANG
    fprintf(txt_file_output, "\nGelandang:\n");
    num_gelandang_terpilih = 0;
    for (int i = 0; i < rating; i++)
    {
        if (strcmp(players[i].position, "Gelandang") == 0 && num_gelandang_terpilih < num_midfielders)
        {
            fprintf(txt_file_output, "%s_%s_%s_%d.png\n", players[i].name, players[i].team, players[i].position, players[i].rating);
            num_gelandang_terpilih++;
        }
    }

    // Print PENYERANG
    fprintf(txt_file_output, "\nPenyerang:\n");
    num_striker_terpilih = 0;
    for (int i = 0; i < rating; i++)
    {
        if (strcmp(players[i].position, "Penyerang") == 0 && num_striker_terpilih < num_strikers)
        {
            fprintf(txt_file_output, "%s_%s_%s_%d.png\n", players[i].name, players[i].team, players[i].position, players[i].rating);
            num_striker_terpilih++;
        }
    }

    // Close output file
    fclose(txt_file_output);

    printf("File closed successfully\n");
}
