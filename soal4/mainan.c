#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include <signal.h>

int main(int argc, char *argv[]) {
    int jam, menit, detik;
    char *file_path;

    if (argc != 5) {
        printf("Usage: %s <jam> <menit> <detik> <file_path>\n", argv[0]);
        exit(1);
    }

    if (strcmp(argv[1], "*") == 0) {
        jam = -1;
    } else {
        jam = atoi(argv[1]);
        if (jam < 0 || jam > 23) {
            printf("Error: jam harus bernilai antara 0 hingga 23\n");
            exit(1);
        }
    }

    if (strcmp(argv[2], "*") == 0) {
        menit = -1;
    } else {
        menit = atoi(argv[2]);
        if (menit < 0 || menit > 59) {
            printf("Error: menit harus bernilai antara 0 hingga 59\n");
            exit(1);
        }
    }

    if (strcmp(argv[3], "*") == 0) {
        detik = -1;
    } else {
        detik = atoi(argv[3]);
        if (detik < 0 || detik > 59) {
            printf("Error: detik harus bernilai antara 0 hingga 59\n");
            exit(1);
        }
    }

    file_path = argv[4];
    pid_t pid = fork();

    if (pid == -1) {
        printf("Error: Failed to fork process\n");
        exit(1);
    } else if (pid > 0) {
        exit(0);
    }

    signal(SIGTERM, exit);

    while (1) {
        time_t now = time(NULL);
        struct tm *tm = localtime(&now);

        if ((jam == -1 || jam == tm->tm_hour) &&
            (menit == -1 || menit == tm->tm_min) &&
            (detik == -1 || detik == tm->tm_sec)) {
            int result = system(file_path);
            if (result != 0) {
                printf("Error: Failed to run the script\n");
            }
        }
        sleep(1);
    }

    return 0;
}
